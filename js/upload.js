class FileUploadManager {
    constructor(fileInputElement, uploadButton, fileDropbox) {
        this.fileInputElement = fileInputElement
        this.fileDropbox = fileDropbox
        this.fileUploadButton = uploadButton
        this.fileList = undefined

        this.fileInputElement.addEventListener("change", (e) => this.handleFiles(e), false)
        this.fileUploadButton.addEventListener("click", 
            (e) => this.fileInputElement.click(), false)
        this.fileDropbox.addEventListener("dragenter", (e) => this.dragenter(e), false);
        this.fileDropbox.addEventListener("dragover", (e) => this.dragover(e), false);
        this.fileDropbox.addEventListener("drop", (e) => this.drop(e), false);
    }

    handleFiles(files) {
        this.fileList = files
        for (let i = 0; i < files.length; i++) {
            const file = files[i];
    
            if (!file.type.startsWith('image/')) { continue }
    
            const img = document.createElement("img");
            img.classList.add("obj");
            img.file = file;
            this.fileDropbox.appendChild(img); // Assuming that "preview" is the div output where the content will be displayed.
    
            const reader = new FileReader();
            reader.onload = (function (aImg) { return function (e) { aImg.src = e.target.result; }; })(img);
            reader.readAsDataURL(file);
        }
    }

    dragenter(e) {
        console.log("Dragenter triggered", e)
        e.stopPropagation();
        e.preventDefault();
    }
    
    dragover(e) {
        console.log("Dragover triggered", e)
        e.stopPropagation();
        e.preventDefault();
    }
    
    drop(e) {
        console.log("Drop triggered", e)
        e.stopPropagation();
        e.preventDefault();
    
        const dt = e.dataTransfer;
        const files = dt.files;
    
        this.handleFiles(files);
    }
}

const fileInputElement = document.getElementById("fileElem")
const fileUploadButton = document.getElementById("fileUploadButton")
const fileDropArea = document.getElementById("dropbox")

const multiUpload = new FileUploadManager(fileInputElement, fileUploadButton, fileDropArea)